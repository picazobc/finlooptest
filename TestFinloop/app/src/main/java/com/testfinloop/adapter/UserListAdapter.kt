package com.testfinloop.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.testfinloop.R
import com.testfinloop.model.UserList
import kotlin.collections.ArrayList


class UserListAdapter(
    private val context : Context,
    private var items : ArrayList<UserList.Data>?,
    private var listener : OnItemClicked
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TAG = "TestFinloop-Adapter"
    private var isSkeleton : Boolean = true
    private val SKELETON_TYPE = -1
    interface OnItemClicked {
        fun onItemClicked(itemClick: UserList.Data?, position: Int)
    }

    fun setItems(items : ArrayList<UserList.Data>?){
        isSkeleton = items==null
        this.items = items
    }

    override fun getItemViewType(position: Int): Int {
        return if(isSkeleton){
            -1
        }else{
            1
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        var view : RecyclerView.ViewHolder
        if(viewType == SKELETON_TYPE){
            view = ViewHolderSeleton(
                (LayoutInflater.from(context).inflate(
                    R.layout.cardview_main_skeleton,
                    parent, false
                ))
            )
        }else{
            view = ViewHolder(
                (LayoutInflater.from(context).inflate(
                    R.layout.item_user,
                    parent, false
                ))
            )
        }
        return view
    }

    override fun getItemCount(): Int {
        if(items!=null){
            items?.let {
                return it.size
            }
        }else{
            return 1
        }
        return 0
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        Log.d(TAG, "onBindViewHolder UsersAdapter started position=$position")
        if(items==null){
            onBindViewSkeletonHolder(holder as ViewHolderSeleton)
        }else{
            onBindViewEventHolder(holder as ViewHolder, position)
        }

    }

    private fun onBindViewSkeletonHolder(
        holder: ViewHolderSeleton) {
        holder.title.text = context.getString(R.string.skeleton_label)
    }

    private fun onBindViewEventHolder(holder: ViewHolder, position: Int) {

        items?.let{ _items ->
            val item = _items[position]
            item?.let{
                val emailString = it.email
                val userNameString = it.username

                holder.email.text = emailString
                holder.userName.text = userNameString

                handleClick(holder.itemView, item,position)
            }
        }
    }

    private fun handleClick(maimClickedView: View, item: UserList.Data?, position: Int) {
        maimClickedView.setOnClickListener {
            item?.let {
                listener.onItemClicked(it, position)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val email: TextView = view.findViewById(R.id.email)
        val userName : TextView = view.findViewById(R.id.userName)
    }

    class ViewHolderSeleton(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.title)
    }

}