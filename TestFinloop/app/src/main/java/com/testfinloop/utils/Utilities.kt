package com.testfinloop.utils

import android.app.Activity
import android.graphics.Color
import cn.pedant.SweetAlert.SweetAlertDialog


class Utilities {

    companion object {
        var pDialog: SweetAlertDialog? = null

        fun showLoad(a: Activity?) {
            pDialog = SweetAlertDialog(a, SweetAlertDialog.PROGRESS_TYPE)
            pDialog?.let {
                it.progressHelper.barColor = Color.parseColor("#4274e2")
                it.titleText = "Cargando"
                it.setCancelable(false)
                it.show()
            }

        }

        fun closeLoad() {
            pDialog?.let {
                it.dismiss()
            }

        }


    }





}