package com.testfinloop.httpreqquest;


import android.content.Context;

import com.testfinloop.R;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Service_retrofit {

    private static Retrofit retrofit = null;
    private Context context;
    private static String BASE_URL = "";


    public Service_retrofit(Context current){
        this.context = current;
        BASE_URL = context.getResources().getString(R.string.a);
    }

    private static Retrofit getRetrofitInstance() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;


    }

    public static Services getApiService() {
        return getRetrofitInstance().create(Services.class);
    }


}