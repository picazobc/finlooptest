package com.testfinloop.httpreqquest;




import com.testfinloop.model.User;
import com.testfinloop.model.UserList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface Services {

    @FormUrlEncoded
    @POST("/users")
    Call<User> registerUser(@Field("email") String email,
                        @Field("username") String username,
                        @Field("password") String password);
    @FormUrlEncoded
    @POST("/login")
    Call<User> login(@Field("email") String email,
                     @Field("password") String password);

    @GET("/users")
    Call<UserList> getUserList(@Header("Authorization") String jwt);

}

