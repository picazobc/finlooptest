package com.testfinloop.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.testfinloop.R
import com.testfinloop.fragment.UsersListFragment


class MainActivity : AppCompatActivity() {

    private val TAG: String = "TestFinloop-Main"
    private var jwtString : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main)

        intent.let {
           jwtString =  it.getStringExtra("jwt")
        }

        val usersListFragment = UsersListFragment.newInstance(jwtString)
        usersListFragment?.let { loadFragment(it) }

    }


    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}