package com.testfinloop.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.testfinloop.R
import com.testfinloop.httpreqquest.Service_retrofit
import com.testfinloop.model.User
import com.testfinloop.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {

    open var appContext: Activity? = this
    var sharedPreferences: SharedPreferences? = null
    var emailRegister : EditText? = null
    var passRegister : EditText? = null
    var userNameRegister : EditText? = null
    var btnRegister : Button? = null
    val VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
        "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
        Pattern.CASE_INSENSITIVE
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        val service = Service_retrofit(this)

        sharedPreferences = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        onActivityCreated()

    }

    private fun onActivityCreated(){
        initViews()
        initListeners()
        initDefaultContent()
    }

    private fun initViews(){
        emailRegister = findViewById(R.id.emailRegister)
        passRegister = findViewById(R.id.passRegister)
        userNameRegister = findViewById(R.id.userNameRegister)
        btnRegister = findViewById(R.id.btnRegister)

    }

    private fun initListeners(){
        btnRegister?.setOnClickListener {
            var email = emailRegister?.text.toString()
            var pass = passRegister?.text.toString()
            var userName = userNameRegister?.text.toString()

            if(validData(email, pass, userName)){
                if(validateEmail(email)){
                    register(email,pass,userName)
                }else{
                    showErrorMessage("Pon una direccion de correo valida")
                }
            }else{
                showErrorMessage("Llena los campos para continuar")
            }
        }

    }
    private fun initDefaultContent(){
        //TODO: Dejo este apartado para hacer modular la aplicacion, aunque sea un test aun puede crecer mas.
    }


    private fun register(email: String?, pass: String?, userName: String?){
        Utilities.showLoad(this)
        val api = Service_retrofit.getApiService()
        val call = api.registerUser(email,userName,pass)
        call.enqueue(object : Callback<User?> {
            override fun onResponse(call: Call<User?>, response: Response<User?>) {
                Utilities.closeLoad()
                if (!response.isSuccessful) {
                    if(!response.message().isNullOrEmpty()){
                        Log.e("ERROR", response.message())
                        showErrorMessage(response.message())
                    }
                    return
                } else {
                    val user = response.body()
                    if (user != null) {
                        saveInfoPreferences(user)
                        goMainActivity(user.jwt)

                    }else{
                        showErrorMessage(appContext!!.getString(R.string.errorGeneral))
                    }

                }
            }

            override fun onFailure(call: Call<User?>, t: Throwable) {}
        })
    }


    private fun goMainActivity(jwt : String?){
        val intent = Intent(this@RegisterActivity, MainActivity::class.java)
        intent.putExtra("jwt", jwt)
        startActivity(intent)
    }

    private fun validData(email: String?, pass: String?, userName: String?): Boolean{
        return !email.isNullOrEmpty() && !pass.isNullOrEmpty() && !userName.isNullOrEmpty()
    }

    private fun showErrorMessage(message: String) {
        SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setTitleText("Error")
            .setContentText(message)
            .show()
    }

    private fun saveInfoPreferences(user: User){
        Log.d("IdUser", user.id)
        Log.d("JWT", user.jwt)

        sharedPreferences?.apply {
            edit().putString("idUser", user.id).apply()
            edit().putString("jwt", user.jwt).apply()
        }
    }

    fun validateEmail(emailStr: String?): Boolean {
        val matcher: Matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr)
        return matcher.find()
    }
}
