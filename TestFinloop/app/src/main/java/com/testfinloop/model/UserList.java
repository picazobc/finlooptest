package com.testfinloop.model;


import java.util.List;

public class UserList {

    private List<Data> users;

    public List<Data> getData() {
        return users;
    }

    public class Data {
        public String _id;
        public String email;
        public String username;
        public String id;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


}
