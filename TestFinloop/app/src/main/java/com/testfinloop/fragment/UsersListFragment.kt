package com.testfinloop.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.testfinloop.R
import com.testfinloop.activity.LoginActivity
import com.testfinloop.adapter.UserListAdapter
import com.testfinloop.httpreqquest.Service_retrofit
import com.testfinloop.model.UserList
import com.testfinloop.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class UsersListFragment : Fragment(), UserListAdapter.OnItemClicked{

    private val TAG: String = "TestFinloop-Frag"
    var currentFragmentView: View? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var usersAdapter : UserListAdapter
    var swipeContainer: SwipeRefreshLayout? = null

    private var userItems : ArrayList<UserList.Data>? = null
    private var jwtString : String? =""

    private var closeSession : FloatingActionButton?  = null
    var preferences : SharedPreferences? = null

    fun UsersListFragment() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                     savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_users_list, container, false)
        preferences = context?.getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)

        currentFragmentView = view
        arguments?.apply {
            jwtString = getString("jwt")
        }
        onFragmentCreated()

        return view
    }

    private fun onFragmentCreated(){
        initViews()
        initListeners()
        initDefaultContent()
    }

    private fun initViews() {
        Log.d(TAG, "initViews started")
        currentFragmentView?.let {
            usersAdapter = UserListAdapter(context!!, userItems , this)
            recyclerView = it.findViewById(R.id.recyclerUsers)
            swipeContainer = it.findViewById(R.id.swipeUsers)
            closeSession = it.findViewById(R.id.closeSession)
        }

    }
    private fun initListeners(){
        Log.d(TAG, "initListeners started")
        swipeContainer?.setOnRefreshListener {
            if (swipeContainer?.isRefreshing!!) {
                swipeContainer?.isRefreshing = false
            }
            getDataUsers()
        }

        closeSession?.setOnClickListener {
            showAlertSession()
        }
    }
    private fun initDefaultContent(){
        Log.d(TAG, "initDefaultContent started")
    }


    override fun onResume() {
        super.onResume()
        getDataUsers()
    }

    private fun getDataUsers() {
        Utilities.showLoad(activity)
        val api = Service_retrofit.getApiService()
        val call = api.getUserList("Bearer $jwtString")
        call.enqueue(object : Callback<UserList?> {
            override fun onResponse(call: Call<UserList?>, response: Response<UserList?>) {
                Utilities.closeLoad()
                if (!response.isSuccessful) {
                    Log.e("ERROR", response.message())
                    showErrorMessage(context!!.getString(R.string.errorGeneral))
                    return
                } else {
                    val response = response.body()
                    if(response != null){
                        var arrayUsers = response?.data as ArrayList<UserList.Data>

                        recyclerView?.apply {
                            onFlingListener = null
                            val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                            layoutAnimation = controller
                            adapter = usersAdapter
                            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL,false)
                        }

                        activity?.let{
                            userItems = arrayUsers
                            usersAdapter?.apply {
                                if(!userItems.isNullOrEmpty()){
                                    setItems(userItems)
                                }else{
                                    setItems(null)
                                }
                                notifyDataSetChanged()
                                recyclerView?.scheduleLayoutAnimation()
                            }
                        }
                    }else{
                        showErrorMessage(context!!.getString(R.string.errorGeneral))
                    }

                }
            }

            override fun onFailure(call: Call<UserList?>, t: Throwable) {}
        })
    }



    private fun showErrorMessage(message: String) {
        SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Error")
                .setContentText(message)
                .show()
    }

    private fun showAlertSession() {
        SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Alerta")
            .setContentText(context?.getString(R.string.alertSession))
            .setCancelText("Cancelar")
            .setConfirmText("Cerrar sesión")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                clearPreferences { cleaningSuccess ->
                    if(cleaningSuccess){
                        val intent = Intent(activity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }else{
                        showErrorMessage("Algo salio mal, intente mas tarde.")
                    }
                }
            }
            .setCancelClickListener { sweetAlertDialog -> sweetAlertDialog.dismissWithAnimation() }
            .show()
    }

    companion object{
        fun newInstance(param1: String?): UsersListFragment? {
            val fragment = UsersListFragment()
            val args = Bundle()
            args.putString("jwt",param1)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onItemClicked(itemClick: UserList.Data?, position: Int) {
        val detailUser = DetailUserFragment.newInstance(itemClick?.username,itemClick?.email,itemClick?._id)
        detailUser?.let { loadFragment(it) }
    }



    private fun loadFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun clearPreferences(onFinish: (Boolean) -> Unit) {
        Log.w(TAG, "clearPreferences started!")
        preferences = activity!!.getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE)
        preferences?.let{ pref ->
            doAsync {
                pref.edit()?.apply{ clear().commit() }
                uiThread {
                    if(pref.all.isEmpty()){
                        Log.w(TAG, "clearPreferences ended")
                        onFinish(true)
                    }else {
                        Log.e(TAG, "clearPreferences failed")
                        onFinish(false)
                    }
                }
            }
        }
    }

}