package com.testfinloop.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.testfinloop.R
import com.testfinloop.adapter.UserListAdapter
import com.testfinloop.httpreqquest.Service_retrofit
import com.testfinloop.model.UserList
import com.testfinloop.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailUserFragment : Fragment(){

    private val TAG: String = "TestFinloop-Frag"
    var currentFragmentView: View? = null
    var emailString : String? = null
    var userNameString : String? = null
    var idString : String? = null
    private var tvEmail : TextView? = null
    private var tvUserName : TextView? = null
    private var tvId : TextView? = null
    private var btnBack : Button? = null


    fun DetailUserFragment() {
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                     savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_user, container, false)

        currentFragmentView = view
        arguments?.apply {
            emailString = getString("email")
            userNameString = getString("userName")
            idString = getString("id")

        }
        onFragmentCreated()

        return view
    }

    private fun onFragmentCreated(){
        initViews()
        initListeners()
        initDefaultContent()
    }

    private fun initViews() {
        Log.d(TAG, "initViews started")
        currentFragmentView?.let {
            tvEmail = it.findViewById(R.id.tvEmail)
            tvUserName = it.findViewById(R.id.tvUserName)
            tvId = it.findViewById(R.id.tvId)
            btnBack = it.findViewById(R.id.btnBack)

        }

    }
    private fun initListeners(){
        Log.d(TAG, "initListeners started")
        btnBack?.setOnClickListener{
            val fm = activity?.supportFragmentManager
            fm?.popBackStack()
        }
    }
    private fun initDefaultContent(){
        Log.d(TAG, "initDefaultContent started")
        tvUserName?.text = userNameString
        tvEmail?.text = emailString
        tvId?.text = idString

    }

    companion object{
        fun newInstance(param1: String?, param2: String?,param3: String?): DetailUserFragment? {
            val fragment = DetailUserFragment()
            val args = Bundle()
            args.putString("userName",param1)
            args.putString("email",param2)
            args.putString("id",param3)
            fragment.arguments = args
            return fragment
        }
    }

}